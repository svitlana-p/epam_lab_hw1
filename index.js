const fs = require('fs');
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const app = express();

const { filesRouter } = require('./filesRouter.js');
const PORT =  8080;


const accessedLog = fs.createWriteStream(path.join(__dirname, 'requestLogs.log'), {flags: 'a'});

app.use(express.json());
app.use(morgan('tiny', { stream: accessedLog }));

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(PORT);
    console.log(`App started ap port: ${PORT}`)
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();

//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  console.error('err')
  res.status(err.status).send({'message': err.message});
}
