// requires...
const fs = require('fs');
const path = require('path');
// constants...
const filesFolder = './files/';

function isFileExtAllowed(filename) {
  const allowed = [ '.log', '.txt', '.json', '.yaml', '.xml', '.js'];
  const fileExt = path.extname(filename);
  return allowed.includes(fileExt);
}

function createFile (req, res, next) {
  // Your code to create the file.
  const { filename, content } = req.body;
  if( filename === undefined || content === undefined ) {
    next({message: 'Please specify parameters. Filename or content is not provided', status: 400})
    return;
  }

  if( !fs.existsSync(filesFolder) ) {
    fs.mkdirSync('files');
  }

  if( !isFileExtAllowed(filename) ) {
    next({message: 'Please specify parameters. Extension of file is not correct', status: 400})
    return;
  }

  const filePath = filesFolder + filename;

  if( fs.existsSync(filePath) ) {
    next({message: `File: ${filename} is already exists`, status: 400})
    return;
  }

  fs.writeFile(filePath, JSON.stringify(content), (err) => {
    if( err ) {
      next({message: 'Failed to create file', status: 500})
      return;
    }
    res.status(200).send({ "message": "File created successfully" });
  })
}

function getFiles (req, res, next) {
  // Your code to get all files.

  fs.readdir(filesFolder, (err, files) => {
    if( err ) {
      next({message: 'Failed to read folder', status: 500})
      return;
    }
    res.status(200).send({
      message: "Success",
      "files": files});
  });

}

const getFile = (req, res, next) => {
  // Your code to get all files.
  const { filename } = req.params;  
  const filePath = filesFolder + filename;

  if( !fs.existsSync(filePath) ) {
    next({message: `No file with '${filename}' filename found`, status: 400})
    return;
  }

  const data = fs.readFileSync( filePath, {encoding:'utf-8', flag: 'r'});
  const {birthtime: uploadedDate} = fs.statSync(filePath);
  

  res.status(200).send({
    message: "Success",
    filename,
    content: JSON.parse(data),
    extension: path.extname(filename).replace('.', ''),
    uploadedDate
  });
}

// Other functions - editFile, deleteFile

const editFile = (req, res, next) => {
  const { filename, content } = req.body;

  if( filename === undefined || content === undefined ) {
    next({message: 'Client error. Filename of content is not provided', status: 400})
    return;
  }

  const filePath = filesFolder + filename;

  if( !fs.existsSync(filePath) ) {
    next({message: `No file with ${filename} filename found`, status: 400})
    return;
  }

  fs.writeFile( filePath, content,{encoding: 'utf-8'}, (err) => {
    res.status(200).send({ "message": `File: ${filename} was successfully edited` });
  })

}

const deleteFile = (req, res, next) => {
  const {filename} = req.params;
  const filePath = filesFolder + filename;
  if( !fs.existsSync(filePath) ) {
    next({message: 'Client error. There is no file with such name', status: 400})
    return;
  }
  fs.unlinkSync(filePath);

  res.status(200).send({"message": `File ${filename} was successfully deleted`})
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
